﻿using Lab7.AddressControl.Contract;
using PK.Container;
using System;
using System.Reflection;

namespace Lab7.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = LabDescriptor.ContainerFactory();
            container.Register(Activator.CreateInstance(LabDescriptor.AddressImpl) as IAddress);
            container.Register(LabDescriptor.RemoteImageControlImpl);

            return container;
        }
    }
}
